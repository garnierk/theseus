package org.polytech.theseus;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class BluetoothDevicesAdapter extends RecyclerView.Adapter<BluetoothDevicesAdapter.ViewHolder> {
    private static final String TAG = "BluetoothDevicesAdapter";
    private final ArrayList<BluetoothDevice> data;
    private final HashSet<String> deviceAddress;
    private final BluetoothService bluetoothService;

    public BluetoothDevicesAdapter(BluetoothService service, ArrayList<BluetoothDevice> data) {
        this.data = data;
        this.bluetoothService = service;
        deviceAddress = new HashSet<>();
    }

    public void add(BluetoothDevice device) {
        if (deviceAddress.contains(device.getAddress())) {
            Log.d(TAG, "Device already registered !");
            return;
        }
        Log.d(TAG, "add");
        int n = data.size();
        data.add(device);
        deviceAddress.add(device.getAddress());
        notifyItemInserted(n);
    }

    public void addAll(Collection<BluetoothDevice> collection) {
        Log.d(TAG, "Add all");
        int start = data.size();
        collection.forEach(device -> {
            if (!deviceAddress.contains(device.getAddress())) {
                deviceAddress.add(device.getAddress());
                data.add(device);
            }
        });
        int end = data.size();
        notifyItemRangeInserted(start, end);
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bluetooth_device_list_item, parent, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        Log.d(TAG, "Set element at " + position);
        if (data.isEmpty()) {
            holder.getTextView().setText(R.string.bt_no_devices_available);
        }
        String name = (data.get(position).getName() == null)? "" : data.get(position).getName();
        holder.getTextView().setText(name + " (" + data.get(position).getAddress() + ")");
        holder.itemView.setOnClickListener(v -> {
            Log.d(TAG, "Input received : " + holder.getAdapterPosition());
            bluetoothService.connect(data.get(position));
            notifyDataSetChanged();
        });
        if (bluetoothService.getConnectedDevice() != null
                && bluetoothService.getConnectedDevice().getAddress().equals(data.get(position).getAddress())) {
            holder.bluetoothLogo.setImageResource(R.drawable.ic_bluetooth_connected);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;
        private final ImageView bluetoothLogo;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.device_name);
            bluetoothLogo = itemView.findViewById(R.id.bluetooth_logo);
        }

        public TextView getTextView() {
            return textView;
        }

        public ImageView getBluetoothLogo() {
            return bluetoothLogo;
        }
    }

}
