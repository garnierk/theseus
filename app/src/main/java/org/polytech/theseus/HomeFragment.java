package org.polytech.theseus;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import org.jetbrains.annotations.NotNull;
import org.polytech.theseus.databinding.FragmentHomeBinding;

import java.nio.charset.StandardCharsets;

public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";
    private final BluetoothHandler handler = new BluetoothHandler(this);
    private FragmentHomeBinding binding;
    private BluetoothAdapter adapter;
    private BluetoothService bluetoothService;
    private ActivityResultLauncher<Intent> enable_bt;
    private Model model;
    private TerminalAdapter terminal;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BluetoothManager manager = getContext().getSystemService(BluetoothManager.class);
        adapter = manager.getAdapter();
        if (adapter == null && getActivity() != null) {
            Toast.makeText(getContext(), R.string.bluetooth_unavailable, Toast.LENGTH_LONG).show();
            getActivity().finish();
        }
        enable_bt = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if (result.getResultCode() == Activity.RESULT_OK) {
                adapter.enable();
                setupBluetoothService();
            } else {
                Log.d(TAG, "onActivityResult() : Bluetooth Not Enabled");
                if (getActivity() != null) {
                    Toast.makeText(getContext(), R.string.bluetooth_not_enabled, Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
            }
        });
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        binding.sendButton.setOnClickListener(v -> sendMessage(binding.cli.getText().toString()));
        binding.terminal.setLayoutManager(new LinearLayoutManager(getContext()));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model = new ViewModelProvider(getActivity()).get(Model.class);
        bluetoothService = model.getBluetoothService().getValue();
        terminal = model.getTerminal().getValue();
        binding.terminal.setAdapter(terminal);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (adapter == null) return;
        if (!adapter.isEnabled()) {
            enable_bt.launch(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
        } else if (bluetoothService == null) {
            setupBluetoothService();
        }
    }

    private void setupBluetoothService() {
        Log.d(TAG, "setupBluetoothService() - start");

        if (getActivity() == null) return;
        binding.cli.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                String message = v.getText().toString();
                sendMessage(message);
            }
            return true;
        });
        if (model.getBluetoothService().getValue() == null) {
            bluetoothService = new BluetoothService(getContext(), handler);
            model.setBluetoothService(bluetoothService);
        }
    }

    private void sendMessage(String text) {
        if (bluetoothService.getState() != BluetoothService.CONNECTED) {
            Toast.makeText(getContext(), R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }
        binding.cli.getText().clear();
        if (text.length() > 0) {
            bluetoothService.write(text.getBytes(StandardCharsets.UTF_8));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void write(byte[] buffer) {
        String message = new String(buffer);
        terminal.append("\n$ " + message);
        binding.terminal.scrollToPosition(terminal.getItemCount() - 1);
    }

    private void read(byte[] buffer) {
        String message = new String(buffer);
        terminal.append("\n" + message);
        binding.terminal.scrollToPosition((terminal.getItemCount() - 1));
    }

    private void setStatus(int state) {
        if (getContext() == null) return;
        String message;
        switch (state) {
            case BluetoothService.CONNECTED:
                message = getString(R.string.device_connected);
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                break;
            case BluetoothService.CONNECTING:
                message = getString(R.string.device_connecting);
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                terminal.clear();
                break;
            case BluetoothService.LISTENING:
            case BluetoothService.NONE:
                message = getString(R.string.device_not_connected);
                break;
            default:
                Log.e(TAG, "Unknown Message");
                message = getString(R.string.device_unknown);
        }
        if (binding != null) {
            terminal.append("\n[i] : " + message);
            binding.terminal.scrollToPosition(terminal.getItemCount() - 1);
        }

    }

    private static class BluetoothHandler extends Handler {
        HomeFragment fragment;

        public BluetoothHandler(HomeFragment fragment) {
            super(Looper.myLooper());
            this.fragment = fragment;
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    fragment.setStatus(msg.arg1);
                    break;
                case Constants.MESSAGE_READ:
                    fragment.read((byte[]) msg.obj);
                case Constants.MESSAGE_WRITE:
                    fragment.write((byte[]) msg.obj);
                    break;
                case Constants.MESSAGE_TOAST:
                    if (fragment.getContext() != null)
                    Toast.makeText(fragment.getContext(), (CharSequence) msg.getData().get(Constants.TOAST), Toast.LENGTH_SHORT)
                            .show();
                    break;
                default:
                    Log.e(TAG, "Bluetooth Handler - handleMessage() : Unknown message");
            }
        }
    }
}