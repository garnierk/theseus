package org.polytech.theseus;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import androidx.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class BluetoothService {

    public final static int NONE = 0, LISTENING = 1, CONNECTING = 2, CONNECTED = 3;
    private final static String TAG = "BluetoothService";
    private static final UUID MY_UUID = UUID.fromString("01c008c7-5591-4ecd-ba35-bde2fd8437f7");
    private final BluetoothAdapter adapter;
    private final Handler handler;
    private final Context context;
    private ConnectThread connectThread;
    private ConnectedThread connectedThread;
    private int state;
    private int newState;
    private BluetoothDevice connectedDevice;

    public BluetoothService(@NonNull Context context, @NonNull Handler handler) {
        BluetoothManager manager = context.getSystemService(BluetoothManager.class);
        adapter = manager.getAdapter();
        state = NONE;
        newState = state;
        this.handler = handler;
        this.context = context;
    }

    private void startLink(BluetoothSocket socket) {
        Log.d(TAG, "startLink() begin \n state = " + state);
        if (connectedThread != null) connectedThread.cancel();
        connectedThread = null;
        connectThread = null;
        if (state == NONE) return;
        connectedThread = new ConnectedThread(socket);
        connectedThread.start();
        updateUI();
    }

    public synchronized void stop() {
        Log.d(TAG, "stop()");
        if (connectedThread != null) connectedThread.cancel();
        if (connectThread != null) connectThread.cancel();
        connectThread = null;
        connectedThread = null;
        state = NONE;
        updateUI();
    }

    public void write(byte[] out) {
        ConnectedThread t;
        synchronized (this) {
            if (state != CONNECTED) return;
            t = connectedThread;
        }
        t.write(out);
    }

    private void notifyConnectionFailed() {
        connectedDevice = null;
        Log.d(TAG, "notify connection failed");
        Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, context.getString(R.string.bt_connection_failed));
        msg.setData(bundle);
        handler.sendMessage(msg);
        state = NONE;
        this.start();
    }

    private void notifyConnectionLost() {
        connectedDevice = null;
        Log.d(TAG, "notify connection lost");
        Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, context.getString(R.string.bt_connection_lost));
        state = NONE;
        msg.sendToTarget();
        updateUI();
        this.start();

    }

    public BluetoothDevice getConnectedDevice() {
        return connectedDevice;
    }

    public void connect(BluetoothDevice device) {
        adapter.cancelDiscovery();
        if (connectedDevice != null && device.getAddress().equals(connectedDevice.getAddress())) {
            Log.i(TAG, "The " + device.getName() + " device is already connected !");
            return;
        }
        Log.d(TAG, "Connect to : " + device.getName());

        if (connectThread != null) connectThread.cancel();
        connectThread = null;

        if (connectedThread != null) {
            connectedThread.cancel();
            connectedThread = null;
        }
        connectThread = new ConnectThread(device);
        connectThread.start();
        updateUI();
        connectedDevice = device;
    }

    private synchronized void updateUI() {
        state = getState();
        Log.d(TAG, "update state " + newState + " with " + state);
        newState = state;

        handler.obtainMessage(Constants.MESSAGE_STATE_CHANGE, newState, -1).sendToTarget();
    }

    public synchronized int getState() {
        return state;
    }

    public synchronized void start() {
        synchronized (this) {
            Log.d(TAG, "start()");
            adapter.enable();
            if (connectThread != null) {
                connectThread = null;
            }

            if (connectedThread != null) {
                connectedThread.cancel();
                connectedThread = null;
            }

            updateUI();
        }
    }

    private class ConnectThread extends Thread {
        private final BluetoothDevice device;
        private BluetoothSocket socket;

        public ConnectThread(BluetoothDevice device) {
            this.device = device;
        }

        @Override
        public void run() {

            if (!adapter.isEnabled()) {
                Log.i(TAG, "Enabling bluetooth...");
                adapter.enable();
                synchronized (this) {
                    while (!adapter.isEnabled()) {
                        try {
                            Log.d(TAG, "Waiting...");
                            wait(100);
                        } catch (InterruptedException e) {
                            Log.e(TAG, "Bluetooth not enabled", e);
                        }
                    }
                }
                Log.i(TAG, "Bluetooth enabled");
            }

            if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                Log.d(TAG, "Create bond...");
                if (!device.createBond()) Log.wtf(TAG, "CreateBound() result: FALSE");
                synchronized (this) {
                    try {
                        wait(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                synchronized (this) {
                    int bounded = CONNECTING;
                    while (bounded == CONNECTING) {
                        switch (device.getBondState()) {
                            case BluetoothDevice.BOND_BONDED:
                                bounded = CONNECTED;
                                Log.d(TAG, "Connected");
                                break;
                            case BluetoothDevice.BOND_NONE:
                                notifyConnectionFailed();
                                return;
                            case BluetoothDevice.BOND_BONDING:
                                try {
                                    Log.d(TAG, "Bonding...");
                                    wait(2000);
                                } catch (InterruptedException e) {
                                    Log.e(TAG, "Device not bounded", e);
                                }
                        }

                    }
                }
                Log.d(TAG, "Bound created");
            }

            BluetoothSocket tmpSocket = null;
            try {
                Log.i(TAG, "Creation of insecure RFCOMM socket start...");
                tmpSocket = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
                Log.i(TAG, "Status : CONNECTED");
                state = CONNECTING;
            } catch (IOException e) {
                Log.e(TAG, "Unable to create socket", e);
                state = NONE;
            } finally {
                socket = tmpSocket;
            }
            if (state == NONE) {
                Log.e(TAG, "Connection failed");
                notifyConnectionFailed();
                return;
            }
            Log.i(TAG, "Connect thread beginning");
            setName("ConnectThread");

            adapter.cancelDiscovery();
            Log.d(TAG, "Device socket creation start...");
            try {
                Log.d(TAG, "Device socket connection start...");
                socket.connect();
                Log.d(TAG, "Device socket connection done");
            } catch (IOException e) {
                Log.e(TAG, "Device socket connection failed", e);
                try {
                    Log.e(TAG, "trying fallback...");
                    socket.close();
                    Log.e(TAG, "socket creation...");
                    socket = (BluetoothSocket) device.getClass().getMethod("createRfcommSocket", new Class[]{int.class}).invoke(device, 1);
                    Log.e(TAG, "socket created");
                    Log.e(TAG, "socket connection...");
                    socket.connect();
                    Log.e(TAG, "socket connected");
                } catch (Exception e1) {

                    Log.e(TAG, "Connection failed", e1);
                    try {
                        socket.close();
                    } catch (IOException ex) {
                        Log.e(TAG, "Can't close the socket", ex);
                    } finally {
                        notifyConnectionFailed();
                    }

                }
            }

            startLink(socket);
        }

        private void cancel() {
            Log.d(TAG, "connectThread - cancel()");
            try {
                if (socket != null) socket.close();
            } catch (IOException e) {
                Log.e(TAG, "connectThread - close() : Unable to close socket", e);
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket socket;
        private final InputStream in;
        private final OutputStream out;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "ConnectedThread constructor");
            this.socket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
                state = CONNECTED;
            } catch (IOException e) {
                Log.e(TAG, "ConnectedThread - Constructor() inputStream getters failed", e);
                state = NONE;
            } finally {
                in = tmpIn;
            }
            try {
                tmpOut = socket.getOutputStream();
                state = (state == NONE) ? NONE : CONNECTED;
            } catch (IOException e) {
                Log.e(TAG, "ConnectedThread - Constructor() outputStream getters failed", e);
            } finally {
                out = tmpOut;
            }
        }

        @Override
        public void run() {
            adapter.enable();
            Log.i(TAG, "ConnectedThread - run() begin");
            byte[] buffer = new byte[1024];
            int bufferLength;

            while (state == CONNECTED) {
                try {
                    bufferLength = in.read(buffer);
                    handler.obtainMessage(Constants.MESSAGE_READ, bufferLength, -1, buffer);
                } catch (IOException e) {
                    state = NONE;
                    Log.e(TAG, "ConnectedThread - connectionLost", e);
                    notifyConnectionLost();
                    break;
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                out.write(buffer);
                handler.obtainMessage(Constants.MESSAGE_WRITE, -1, -1, buffer).sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "ConnectedThread - write() : An error occurred during write", e);
                Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TOAST, context.getString(R.string.data_not_send));
                state = NONE;
                msg.sendToTarget();
            }
        }

        public void cancel() {
            try {
                socket.close();
            } catch (IOException e) {
                Log.e(TAG, "ConnectedThread - cancel() : Unable to close the socket", e);
            }
        }
    }
}
