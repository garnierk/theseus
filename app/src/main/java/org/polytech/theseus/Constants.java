package org.polytech.theseus;

public interface Constants {

    int MESSAGE_STATE_CHANGE = 0;
    int MESSAGE_READ = 1;
    int MESSAGE_WRITE = 2;
    int MESSAGE_TOAST = 4;

    String TOAST = "toast";

}
