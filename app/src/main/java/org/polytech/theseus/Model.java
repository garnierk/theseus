package org.polytech.theseus;

import android.bluetooth.BluetoothDevice;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class Model extends ViewModel {
    private final MutableLiveData<BluetoothService> bluetoothService;
    private final MutableLiveData<ArrayList<BluetoothDevice>> bluetoothDevices;
    private final MutableLiveData<BluetoothDevicesAdapter> bluetoothDevicesAdapter;
    private final MutableLiveData<TerminalAdapter> terminal;
    private final MutableLiveData<FloatingActionButton> scanButton;

    public Model() {
        bluetoothDevices = new MutableLiveData<>(new ArrayList<>());
        bluetoothService = new MutableLiveData<>(null);
        bluetoothDevicesAdapter = new MutableLiveData<>();
        terminal = new MutableLiveData<>(new TerminalAdapter(new ArrayList<>()));
        scanButton = new MutableLiveData<>();
    }

    public LiveData<BluetoothService> getBluetoothService() {
        return bluetoothService;
    }

    public void setBluetoothService(BluetoothService bluetoothService) {
        this.bluetoothService.setValue(bluetoothService);
    }

    public LiveData<ArrayList<BluetoothDevice>> getBluetoothDevices() {
        return bluetoothDevices;
    }

    public LiveData<BluetoothDevicesAdapter> getBluetoothDevicesAdapter() {
        return bluetoothDevicesAdapter;
    }

    public void setBluetoothDevicesAdapter(BluetoothDevicesAdapter adapter) {
        bluetoothDevicesAdapter.setValue(adapter);
    }

    public LiveData<TerminalAdapter> getTerminal() {
        return terminal;
    }

    public LiveData<FloatingActionButton> getScanButton() {
        return scanButton;
    }

    public void setScanButton(FloatingActionButton scanButton) {
        this.scanButton.setValue(scanButton);
    }
}
