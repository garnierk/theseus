package org.polytech.theseus;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import org.polytech.theseus.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    Model model;
    private BluetoothAdapter adapter;
    private final ActivityResultLauncher<Intent> enable_bt = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            Log.d(TAG, "Bluetooth change detected");
            if (result.getResultCode() == Activity.RESULT_OK) {
                adapter.enable();
            } else {
                Log.d(TAG, "onActivityResult() : Bluetooth Not Enabled");
                Toast.makeText(getApplicationContext(), R.string.bluetooth_not_enabled, Toast.LENGTH_LONG).show();
                finish();
            }
        }
    });

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "Action received : " + action);
            switch (action) {
                case BluetoothDevice.ACTION_FOUND:
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    model.getBluetoothDevicesAdapter().getValue().add(device);
                    break;
                case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                    model.getScanButton().getValue().setImageResource(R.drawable.ic_scan);
                    break;
                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    if (adapter.getState() == BluetoothAdapter.STATE_OFF
                            || adapter.getState() == BluetoothAdapter.STATE_TURNING_OFF) {
                        enable_bt.launch(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
                        break;
                    }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BluetoothManager manager = getSystemService(BluetoothManager.class);
        adapter = manager.getAdapter();
        org.polytech.theseus.databinding.ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        model = new ViewModelProvider(this).get(Model.class);
        model.getTerminal().getValue().append(getString(R.string.welcome_text));
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_bluetooth)
                .build();
        ActionBar actionBar = getSupportActionBar();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setCustomView(R.layout.custom_action_bar);

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(receiver, filter);
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(receiver, filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (adapter != null) adapter.cancelDiscovery();
        unregisterReceiver(receiver);
        model.getBluetoothService().getValue().stop();
    }

    public void discover() {
        Log.d(TAG, "Discovery begin");
        if (!adapter.isEnabled()) {
            enable_bt.launch(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
            return;
        }

        int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!isGpsEnabled) {
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> discover())
                    .launch(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
        if (adapter.isDiscovering()) adapter.cancelDiscovery();
        if (!adapter.startDiscovery()) {
            Log.wtf(TAG, "Unable to start discovery");
            model.getScanButton().getValue().setImageResource(R.drawable.ic_scan);
            Toast.makeText(this, R.string.scan_error, Toast.LENGTH_LONG).show();
        }
    }

}