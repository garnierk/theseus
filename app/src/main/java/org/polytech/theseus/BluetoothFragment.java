package org.polytech.theseus;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import org.jetbrains.annotations.NotNull;
import org.polytech.theseus.databinding.FragmentBluetoothBinding;

import java.util.ArrayList;

public class BluetoothFragment extends Fragment {

    private FragmentBluetoothBinding binding;
    private BluetoothAdapter adapter;
    private ArrayList<BluetoothDevice> data;

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BluetoothManager manager = getContext().getSystemService(BluetoothManager.class);
        adapter = manager.getAdapter();
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = FragmentBluetoothBinding.inflate(inflater, container, false);
        binding.scanButton.setOnClickListener(v -> {
            binding.scanButton.setImageResource(R.drawable.ic_loading);
            ((MainActivity) getActivity()).discover();
        });
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Model model = new ViewModelProvider(getActivity()).get(Model.class);
        BluetoothService bluetoothService = model.getBluetoothService().getValue();
        data = model.getBluetoothDevices().getValue();
        BluetoothDevicesAdapter viewAdapter = new BluetoothDevicesAdapter(bluetoothService, data);
        binding.recyclerView.setAdapter(viewAdapter);

        model.setBluetoothDevicesAdapter(viewAdapter);
        viewAdapter.addAll(adapter.getBondedDevices());
        model.setScanButton(binding.scanButton);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        data.clear();
    }
}

